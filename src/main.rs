use std::net::TcpListener;
use std::io::{BufRead, BufReader};
use std::io::Write;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:12345").expect("couldn't bind to address");
    for stream in listener.incoming(){
        match stream {
            Ok(socket) => {
                let mut socket_dup = socket.try_clone().unwrap();
                println!("new client: {:?}", socket.peer_addr().unwrap());
                let buf = BufReader::new(socket);
                for line in buf.lines() {
                    if line.as_ref().unwrap() == "quit" {
                        break
                    }
                    write!(socket_dup, "{}\n", line.unwrap());
                }
            },
            Err(e) => println!("couldn't get client: {:?}", e),
        }
    }
}
